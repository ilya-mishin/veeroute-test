"use strict";

var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    notify = require("gulp-notify"),
    bower = require('gulp-bower');

var config = {
    srcPaths: {
        sassPath: './sass',
        bowerDir: './bower_components'
    },
    buildPaths: {
        assets: './public/assets',
        js: './public/js',
        css: './public/css'
    }
};

gulp.task('bower', function() {
    return bower().pipe(gulp.dest(config.srcPaths.bowerDir));
});

gulp.task('css', function() {
    return sass(config.srcPaths.sassPath + '/style.scss', {
        style: 'compressed',
        loadPath: [config.srcPaths.sassPath]
    }).pipe(gulp.dest(config.buildPaths.css));
});

gulp.task('watch', function() {
    gulp.watch(config.srcPaths.sassPath + '/*.scss', ['css']);
});

gulp.task('angularAsset', function() {
    gulp.src([
        config.srcPaths.bowerDir + '/angular/angular.min.js',
        config.srcPaths.bowerDir + '/angular/angular.min.js.gzip',
        config.srcPaths.bowerDir + '/angular/angular.min.js.map'
    ], {
        base: config.srcPaths.bowerDir
    }).pipe(gulp.dest(config.buildPaths.assets));
});

gulp.task('bootstrapAsset', function() {
    gulp.src([
        config.srcPaths.bowerDir + '/bootstrap-css-only/css/bootstrap.min.css',
        config.srcPaths.bowerDir + '/bootstrap-css-only/css/bootstrap.min.css.map',
        config.srcPaths.bowerDir + '/bootstrap-css-only/css/bootstrap-theme.min.css',
        config.srcPaths.bowerDir + '/bootstrap-css-only/css/bootstrap-theme.min.css.map',
        config.srcPaths.bowerDir + '/bootstrap-css-only/fonts/*'
    ], {
        base: config.srcPaths.bowerDir
    }).pipe(gulp.dest(config.buildPaths.assets));
});

gulp.task('weatherIconsAsset', function() {
    gulp.src([
        config.srcPaths.bowerDir + '/weather-icons/css/weather-icons.min.css',
        config.srcPaths.bowerDir + '/weather-icons/font/*'
    ], {
        base: config.srcPaths.bowerDir
    }).pipe(gulp.dest(config.buildPaths.assets));
});

gulp.task('default', [
    'bower',
    'css',
    'angularAsset',
    'bootstrapAsset',
    'weatherIconsAsset'
]);