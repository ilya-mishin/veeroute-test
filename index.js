"use strict";

var express = require('express');
var app = express();
var fs = require('fs');

app.use(express.static('public'));

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});

function generate() {
    var flagList = ["lightning", "alien", "meteor", "cloud"],
        result = {
            "left": [],
            "right": []
        };

    for (let i = 0; i < 100; i++) {
        let shuffledFlags = flagList.sort(function() {
            return 0.5 - Math.random();
        });

        result.left.push({
            name: 'item l.' + i,
            flags: [shuffledFlags[0], shuffledFlags[1]]
        });

        result.right.push({
            name: 'item r.' + i,
            flags: [shuffledFlags[2], shuffledFlags[3]]
        });
    }

    fs.writeFile('./public/js/data.json', JSON.stringify(result), function (err) {
        if (err) return console.log(err);
        console.log('data.json created');
    });
}

generate();