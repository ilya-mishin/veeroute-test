"use strict";

var app = angular.module('app', []);

app.controller('ItemListController', function ItemListController($q, $scope, $http) {
    // сортировка по возрастанию убыванию
    $scope.reverse = false;
    // поиск элемента по названию
    $scope.searchText = '';

    // доступные флаги
    $scope.flagsList = ["lightning", "alien", "meteor", "cloud"];

    $scope.setCurrentItem = function (item) {
        $scope.currentItem = item;
    };

    $scope.setCurrentFlag = function (flag) {
        $scope.selectedFlag = $scope.selectedFlag == flag ? undefined : flag;
    };

    // получение списка элементов
    $http.get('/js/data.json').success(function(data) {
        $scope.items = data;
    });
});